Title: OpenSSL 1.1 upgrade procedure
Author: Marc-Antoine Perennou <keruspe@exherbo.org>
Content-Type: text/plain
Posted: 2018-12-03
Revision: 2
News-Item-Format: 1.0
Display-If-Installed: dev-libs/openssl[<1.1]

openssl 1.1 has been unmasked.
As openssl is not (yet) slotted, you need to follow this procedure
in order not to break your system entirely.
If you're short on time, you can keep this for later by masking it locally:

    echo 'dev-libs/openssl[>=1.1]' >> /etc/paludis/package_mask.conf

Make sure we have all the sources tarballs locally as wget will break:

    cave resolve world -c --fetch -x
    cave resolve wget --fetch -x

Now update openssl:

    cave resolve -x1 openssl

Now repair libarchive which is used by paludis:
(Note that you can't generate pbins before this step is done.)

    cave resolve -x1z libarchive

Now let's fix what's broken.
We skip tests as some will fail as the system is in a transitional state.
You might need to run this several times because of some failures.

    cave fix-linkage -x -- -Cs --skip-phase test

Now that either everything was fixed or you just get the same failure at each
fix-linkage, rebuild everything that depend on openssl. Some features might have
been mis-detected during the transition so we want to make sure everything is ok.

    cave resolve nothing --reinstall-dependents-of openssl -Cs -x
