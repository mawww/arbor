# Copyright 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=mity tag=release-${PV} ] cmake

SUMMARY="A Markdown parser implementation in C"
DESCRIPTION="
It has the following features:
* Compliance: Generally MD4C aims to be compliant to the latest version of
  CommonMark specification. Right now it's fully compliant to CommonMark 0.29.
* Extensions: MD4C supports some commonly requested and accepted extensions.
* Compactness: MD4C is implemented in one source file and one header file.
* Embedding: MD4C is easy to reuse in other projects, its API is very
  straightforward: There is actually just one function, md_parse().
* Push model: MD4C parses the complete document and calls callback functions
  provided by the application for each start/end of block, start/end of a span,
  and with any textual contents.
* Portability: MD4C builds and works on Windows and Linux, and it should be
  fairly simple to make it run also on most other systems.
* Encoding: MD4C can be compiled to recognize ASCII-only control characters,
  UTF-8 and, on Windows, also UTF-16, i.e. what is on Windows commonly called
  just \"Unicode\". See more details below.
* Performance: MD4C is very fast.
"

LICENCES="MIT"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES=""

CMAKE_SRC_CONFIGURE_PARAMS+=( -DBUILD_SHARED_LIBS:BOOL=TRUE )

