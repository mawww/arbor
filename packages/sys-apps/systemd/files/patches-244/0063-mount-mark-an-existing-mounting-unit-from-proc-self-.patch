From e6d694254fe115cc04852732172959998e051b87 Mon Sep 17 00:00:00 2001
From: Jun'ichi Nomura <j-nomura@ce.jp.nec.com>
Date: Tue, 3 Dec 2019 16:52:02 +0900
Subject: [PATCH 063/104] mount: mark an existing "mounting" unit from
 /proc/self/mountinfo as "just_mounted"

When starting a mount unit, systemd invokes mount command and moves the
unit's internal state to "mounting".  Then it watches for updates of
/proc/self/mountinfo.  When the expected mount entry newly appears in
mountinfo, the unit internal state is changed to "mounting-done".
Finally, when systemd finds the mount command has finished, it checks
whether the unit internal state is "mounting-done" and changes the state
to "mounted".
If the state was not "mounting-done" in the last step though mount command
was successfully finished, the unit is marked as "failed" with following
log messages:
  Mount process finished, but there is no mount.
  Failed with result 'protocol'.

If daemon-reload is done in parallel with starting mount unit, it is
possible that things happen in following order and result in above failure.
  1. the mount unit state changes to "mounting"
  2. daemon-reload saves the unit state
  3. kernel completes the mount and /proc/self/mountinfo is updated
  4. daemon-reload restores the saved unit state, that is "mounting"
  5. systemd notices the mount command has finished but the unit state
     is still "mounting" though it should be "mounting-done"

mount_setup_existing_unit() should take into account that MOUNT_MOUNTING
is transitional state and set MOUNT_PROC_JUST_MOUNTED flag if the unit
comes from /proc/self/mountinfo so that mount_process_proc_self_mountinfo()
later can make state transition from "mounting" to "mounting-done".

Fixes: #10872
(cherry picked from commit 1d086a6e59729635396204fc05234f1d3caa0847)
---
 src/core/mount.c | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/src/core/mount.c b/src/core/mount.c
index dfed691c43..9f88afc411 100644
--- a/src/core/mount.c
+++ b/src/core/mount.c
@@ -1568,7 +1568,7 @@ static int mount_setup_existing_unit(
         if (r > 0)
                 flags |= MOUNT_PROC_JUST_CHANGED;
 
-        if (!MOUNT(u)->from_proc_self_mountinfo || FLAGS_SET(MOUNT(u)->proc_flags, MOUNT_PROC_JUST_MOUNTED))
+        if (!MOUNT(u)->from_proc_self_mountinfo || FLAGS_SET(MOUNT(u)->proc_flags, MOUNT_PROC_JUST_MOUNTED) || MOUNT(u)->state == MOUNT_MOUNTING)
                 flags |= MOUNT_PROC_JUST_MOUNTED;
 
         MOUNT(u)->from_proc_self_mountinfo = true;
-- 
2.25.1

